<p align="center">
    <h1 align="center">Ejemplo numero 1 de Yii2</h1>
    <br>
</p>

En este ejemplo vamos a partir del layout base y vamos a trabajar con los siguientes elementos:
<ul>
<li>Descargar la aplicación</li>
<li>Configurar una aplicación</li>
<li>Entender el funcionamiento de los controladores y de las acciones</li>
<li>Ver como se acceden a las vistas</li>
<li>Entender lo que es un layout</li>
<li>Configurar el menú para poder acceder a las distintas paginas</li>
<li>Como cargar las hojas de estilos y los javascripts nuestros</li>
<li>Entender los assets</li>
<li>Conectarse a una base de datos y realizar una consulta</li>
</ul>

INSTALACION
------------

### INSTALANDO MEDIANTE GIT

Teniendo Gii instalado debeis escribir el siguiente comando:

~~~
git clone https://gitlab.com/ramonabramogrupo/cursodesarrollo2018/mvcyii2/ejemplos/ejemplo1.git ejemplo1
~~~

Puedes elegir un nombre de directorio diferente si así lo deseas, por defecto es ejemplo1.

### INSTALANDO MEDIANTE COMPOSER

Teniendo composer instalado debeis escribir el siguiente comando:

~~~
composer create-project ramonabramogrupo/ejemplo1-yii2 ejemplo1
~~~

Puedes elegir un nombre de directorio diferente si así lo deseas, por defecto es ejemplo1.
